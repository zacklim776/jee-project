package fr.uge.jee.reddit;

import java.util.regex.Pattern;

public class PasswordManager {
    @javax.validation.constraints.Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$")
    private String newP;
    private String confirmP;

    public String getNewP() {
        return newP;
    }

    public void setNewP(String newP) {
        this.newP = newP;
    }

    public String getConfirmP() {
        return confirmP;
    }

    public void setConfirmP(String confirmP) {
        this.confirmP = confirmP;
    }

    public static boolean checkPasswordRegexp(String password) {
        var p = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$");
        var m = p.matcher(password);
        return m.matches();
    }

    public boolean checkIfSimilar() {
        return newP.equals(confirmP);
    }

}
