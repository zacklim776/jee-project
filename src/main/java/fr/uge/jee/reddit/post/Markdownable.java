package fr.uge.jee.reddit.post;

import fr.uge.jee.reddit.service.markdown.MarkdownService;
import fr.uge.jee.reddit.service.markdown.MarkdownServiceImpl;

public interface Markdownable {
    MarkdownService markdownService = new MarkdownServiceImpl();
}
