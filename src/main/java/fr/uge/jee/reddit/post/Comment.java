package fr.uge.jee.reddit.post;

import fr.uge.jee.reddit.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static fr.uge.jee.reddit.Application.timeFormatBuilder;

@Entity
@Table(name="COMMENT")
public class Comment implements Serializable, DisplayedContent, Markdownable {
    @Id
    @GeneratedValue(generator = "commentId")
    private long commentId;

    @ManyToOne
    private User author;

    @NotEmpty(message = "Please write some content")
    private String content;

    private String formattedContent;

    private boolean deleted;

    @ManyToOne(fetch = FetchType.LAZY)
    private Comment father;

    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "father")
    private List<Comment> children = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    private Post origin;

    @Embedded
    private VoteManager voteManager = new VoteManager();

    private long timestamp = Timestamp.from(Instant.now()).getTime();

    @Version
    private Long version;

    public Comment(User author, String content, Comment father, long timestamp) {
        Objects.requireNonNull(author);
        Objects.requireNonNull(content);
        this.author = author;
        this.content = markdownService.markdownToPlainText(content);
        this.formattedContent = markdownService.markdownToHtml(content);
        this.father = father;
        this.timestamp = timestamp;
    }

    public Comment() {
    }

    public long getCommentId() {
        return this.commentId;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Comment getFather() {
        return father;
    }

    public void setFather(Comment father) {
        this.father = father;
    }

    public List<Comment> getChildren() {
        return children;
    }

    public void setChildren(List<Comment> children) {
        this.children = children;
    }

    public int getScore() {
        return voteManager.getScore();
    }

    public void setVoteManager(VoteManager voteManager) {
        this.voteManager = voteManager;
    }

    public VoteManager getVoteManager() {
        return this.voteManager;
    }

    public void addUpvote(int userId) {
        this.voteManager.addUpvote(userId);
    }

    public void addDownVote(int userId) {
        this.voteManager.addDownVote(userId);
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Post getOrigin() {
        return origin;
    }

    public void setOrigin(Post origin) {
        this.origin = origin;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String displayCreationDate() {
        return timeFormatBuilder(timestamp);
    }

    public String getFormattedContent() {
        return formattedContent;
    }

    public void setFormattedContent(String formattedContent) {
        this.formattedContent = formattedContent;
    }
}
