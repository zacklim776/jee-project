package fr.uge.jee.reddit.post;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.HashSet;

@Embeddable
public class VoteManager implements Serializable {

    @Column(length = 1000000)
    private HashSet<Integer> upvotesSet;
    @Column(length = 1000000)
    private HashSet<Integer> downvotesSet;

    public VoteManager() {
        this.upvotesSet = new HashSet<>();
        this.downvotesSet = new HashSet<>();
    }

    private static void vote(int userId, HashSet<Integer> v1, HashSet<Integer> v2) {
        if (v1.contains(userId)) {
            v1.remove(userId);
        } else if (v2.contains(userId)) {
            v1.add(userId);
            v2.remove(userId);
        } else {
            v1.add(userId);
        }
    }

    public void addUpvote(int userId) {
        vote(userId, upvotesSet, downvotesSet);
    }

    public void addDownVote(int userId) {
        vote(userId, downvotesSet, upvotesSet);
    }

    public HashSet<Integer> getupvotesSet() {
        return upvotesSet;
    }

    public void setupvotesSet(HashSet<Integer> upvotesSet) {
        this.upvotesSet = upvotesSet;
    }

    public HashSet<Integer> getDownvotes() {
        return downvotesSet;
    }

    public void setDownvotes(HashSet<Integer> downvotes) {
        this.downvotesSet = downvotes;
    }

    public int getScore() {
        return upvotesSet.size() - downvotesSet.size();
    }
}
