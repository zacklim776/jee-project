package fr.uge.jee.reddit.post;

import fr.uge.jee.reddit.service.markdown.MarkdownService;
import fr.uge.jee.reddit.service.markdown.MarkdownServiceImpl;
import fr.uge.jee.reddit.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;

import static fr.uge.jee.reddit.Application.timeFormatBuilder;

@Entity
public class Post implements Serializable, Markdownable {
    @Id
    @GeneratedValue(generator = "postId")
    private long postId;

    @NotEmpty(message = "Please write a title")
    private String title;

    @ManyToOne(fetch = FetchType.LAZY)
    private User author;

    @NotEmpty(message = "Please write some content")
    private String content;

    private String formattedContent;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, mappedBy = "origin")
    private List<Comment> comments = new ArrayList<>();

    @Embedded
    private VoteManager voteManager = new VoteManager();

    @Version
    private Long version;

    private long timestamp = Timestamp.from(Instant.now()).getTime();

    public Post(String title, User author, String content, long timestamp) {
        Objects.requireNonNull(title);
        Objects.requireNonNull(author);
        Objects.requireNonNull(content);
        this.title = title;
        this.author = author;
        this.content = markdownService.markdownToPlainText(content);
        this.formattedContent = markdownService.markdownToHtml(content);
        this.timestamp = timestamp;
    }

    public Post(List<Comment> comments) {
        this.comments = comments;
    }

    public Post() {
    }

    public long getPostId() {
        return postId;
    }

    public void setPostId(long postId) {
        this.postId = postId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public void addComment(Comment comment) {
        this.comments.add(comment);
    }

    public int getScore() {
        return voteManager.getScore();
    }

    public void setVoteManager(VoteManager voteManager) {
        this.voteManager = voteManager;
    }

    public VoteManager getVoteManager() {
        return this.voteManager;
    }

    public void addUpvote(int userId) {
        this.voteManager.addUpvote(userId);
    }

    public void addDownVote(int userId) {
        this.voteManager.addDownVote(userId);
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String displayCreationDate() {
        return timeFormatBuilder(timestamp);
    }

    public String getFormattedContent() {
        return formattedContent;
    }

    public void setFormattedContent(String formattedContent) {
        this.formattedContent = formattedContent;
    }

    public int getNumberOfComments() {
        return comments.size();
    }
}
