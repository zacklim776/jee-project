package fr.uge.jee.reddit.post;

public interface DisplayedContent {
    long getTimestamp();
    int getScore();
}
