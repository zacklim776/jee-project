package fr.uge.jee.reddit.repository;

import fr.uge.jee.reddit.dto.PostDTO;
import fr.uge.jee.reddit.post.Comment;
import fr.uge.jee.reddit.post.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends PagingAndSortingRepository<Post, Long> {
    @Query("SELECT p from Post p LEFT JOIN FETCH p.comments")
    List<Post> loadPosts();

    @Query("SELECT p from Post p LEFT JOIN FETCH p.comments where p.postId=:postId")
    Post loadPost(long postId);

    @Query("SELECT p.comments from Post p where p.postId=:postId")
    List<Comment> loadComments(long postId);

    @Query("SELECT p FROM Post p LEFT JOIN FETCH p.comments WHERE p.author.username=:opUsername ")
    List<Post> findPostsByUser(String opUsername);

    @Query("SELECT new fr.uge.jee.reddit.dto.PostDTO(p.id, p.title, p.author, p.content, p.voteManager, p.timestamp, SIZE(p.comments)) from Post p")
    Page<PostDTO> loadPostDTO(Pageable pageable);

    @Query("SELECT new fr.uge.jee.reddit.dto.PostDTO(p.id, p.title, p.author, p.content, p.voteManager, p.timestamp, SIZE(p.comments)) from Post p ORDER BY p.timestamp DESC")
    Page<PostDTO> loadPostDTOSortByTime(Pageable pageable);

    @Query("SELECT new fr.uge.jee.reddit.dto.PostDTO(p.id, p.title, p.author, p.content, p.voteManager, p.timestamp, SIZE(p.comments)) from Post p where p.id=:postId")
    PostDTO loadPostDTO(long postId);

    @Query("SELECT new fr.uge.jee.reddit.dto.PostDTO(p.id, p.title, p.author, p.content, p.voteManager, p.timestamp, SIZE(p.comments)) FROM Post p WHERE p.author.username=:opUsername ")
    List<PostDTO> findPostsDTOByUser(String opUsername);
}
