package fr.uge.jee.reddit.repository;

import fr.uge.jee.reddit.post.Comment;
import fr.uge.jee.reddit.post.Post;
import fr.uge.jee.reddit.user.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface UserRepository extends CrudRepository<User, String> {
    @Query("SELECT u from users u where u.username=:login")
    User findByUsername(String login);

    //Method for testing
    @Query("SELECT u.comments from users u where u.username=:username")
    ArrayList<Comment> findCommentsByUser(String username);
}
