package fr.uge.jee.reddit.repository;

import fr.uge.jee.reddit.dto.CommentDTO;
import fr.uge.jee.reddit.post.Comment;
import fr.uge.jee.reddit.post.Post;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Long> {
    @Query("Select DISTINCT c from Comment c LEFT JOIN FETCH c.children WHERE c.origin=:post")
    List<Comment> getCommentsByPost(Post post);

    @Query("SELECT new fr.uge.jee.reddit.dto.CommentDTO(c.id, c.author, c.content, c.deleted, c.voteManager, c.timestamp) from Comment c where c.id=:commentId")
    CommentDTO loadCommentDTO(long commentId);

    @Query("SELECT c FROM Comment c LEFT JOIN FETCH c.origin WHERE c.author.username=:opUsername ")
    List<Comment> findCommentsByUser(String opUsername);

    @Query("SELECT new fr.uge.jee.reddit.dto.CommentDTO(c.id, c.author, c.content, c.deleted, c.voteManager, c.timestamp) from Comment c where c.author.username=:opUsername")
    List<CommentDTO> findCommentsDTOByUser(String opUsername);
}
