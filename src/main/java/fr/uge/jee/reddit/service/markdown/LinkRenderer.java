package fr.uge.jee.reddit.service.markdown;

import org.commonmark.node.AbstractVisitor;
import org.commonmark.node.Link;
import org.commonmark.node.Node;
import org.commonmark.renderer.NodeRenderer;
import org.commonmark.renderer.text.TextContentNodeRendererContext;

import java.util.Collections;
import java.util.Set;

public class LinkRenderer extends AbstractVisitor implements NodeRenderer {
    private final TextContentNodeRendererContext context;

    public LinkRenderer(TextContentNodeRendererContext context) {
        this.context = context;
    }

    @Override
    public Set<Class<? extends Node>> getNodeTypes() {
        return Collections.<Class<? extends Node>>singleton(Link.class);
    }

    @Override
    public void render(Node node) {
        renderChildren(node);
    }

    private void renderChildren(Node parent) {
        var node = parent.getFirstChild();
        while (node != null) {
            var next = node.getNext();
            context.render(node);
            node = next;
        }
    }
}
