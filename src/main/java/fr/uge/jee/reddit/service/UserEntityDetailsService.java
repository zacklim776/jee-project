package fr.uge.jee.reddit.service;

import fr.uge.jee.reddit.post.Post;
import fr.uge.jee.reddit.repository.UserRepository;
import fr.uge.jee.reddit.user.User;
import fr.uge.jee.reddit.user.UserEntityDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import javax.transaction.Transactional;

@Service
public class UserEntityDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserEntityDetailsServiceFailure userEntityDetailsServiceFailure;

    @PersistenceContext
    private EntityManager em;

    public UserEntityDetailsService() {
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new UserEntityDetails(user);
    }

    public boolean insertUser(User user) {
        var res = false;
        var retry = true;
        while(retry) {
            retry = false;
            try {
                res = userEntityDetailsServiceFailure.insertUser(user);
            } catch (org.springframework.orm.ObjectOptimisticLockingFailureException e ){
                retry = true;
            }
        }
        return res;
    }

    @Transactional
    public User findUser(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return user;
    }

    public void addPost(Post post, User user) {
        var retry = true;
        while(retry) {
            retry = false;
            try {
                userEntityDetailsServiceFailure.addPost(post, user);
            } catch (org.springframework.orm.ObjectOptimisticLockingFailureException e ){
                retry = true;
            }
        }
    }

    public void updatePassword(String username, String np) {
        var retry = true;
        while(retry) {
            retry = false;
            try {
                userEntityDetailsServiceFailure.updatePassword(username, np);
            } catch (org.springframework.orm.ObjectOptimisticLockingFailureException e ){
                retry = true;
            }
        }
    }
}
