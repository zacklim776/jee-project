package fr.uge.jee.reddit.service;

import fr.uge.jee.reddit.dto.CommentDTO;
import fr.uge.jee.reddit.enums.SortCriteria;
import fr.uge.jee.reddit.post.Comment;
import fr.uge.jee.reddit.post.Post;
import fr.uge.jee.reddit.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CommentService {

    @Autowired
    CommentRepository commentRepository;

    @Autowired
    CommentServiceFailure commentServiceFailure;

    @PersistenceContext
    EntityManager em;

    public CommentService() {
    }

    public List<Comment> findCommentsByUser(String author) {
        return commentRepository.findCommentsByUser(author);
    }

    public List<CommentDTO> findCommentsDTOByUser(String author) {
        return commentRepository.findCommentsDTOByUser(author);
    }

    public void addComment(Comment comment) {
        var retry=true;
        while(retry) {
            retry=false;
            try {
                commentServiceFailure.addComment(comment);
            } catch (org.springframework.orm.ObjectOptimisticLockingFailureException e){
                retry=true;
            }
        }
    }

    public void addUpvote(long id, String username) {
//        var retry=true;
//        while(retry) {
//            retry=false;
//            try {
//                commentServiceFailure.addUpvote(id, username);
//            } catch (org.springframework.orm.ObjectOptimisticLockingFailureException e){
//                retry=true;
//            }
//        }
        commentServiceFailure.addUpvote(id, username);
    }

    public void addDownvote(long id, String username) {
//        var retry=true;
//        while(retry) {
//            retry=false;
//            try {
//                commentServiceFailure.addDownvote(id, username);
//            } catch (org.springframework.orm.ObjectOptimisticLockingFailureException e){
//                retry=true;
//            }
//        }
        commentServiceFailure.addDownvote(id, username);
    }

    @Transactional
    public Optional<Comment> getCommentById(long commentId) {
        return commentRepository.findById(commentId);
    }

    @Transactional
    public CommentDTO getCommentDTOById(long commentId) {
        return commentRepository.loadCommentDTO(commentId);
    }


    @Transactional
    public List<Comment> loadCommentsByPost(Post post, SortCriteria sortCriteria) {
        switch(sortCriteria) {
            case CONTROVERSY:
            case TIME: return commentRepository.getCommentsByPost(post).stream()
                    .filter(c -> c.getFather()==null)
                    .sorted(PostService::compareByTime)
                    .collect(Collectors.toList());
            case SCORE: return commentRepository.getCommentsByPost(post).stream()
                    .filter(c -> c.getFather()==null)
                    .sorted(PostService::compareByScore)
                    .collect(Collectors.toList());
            default: return commentRepository.getCommentsByPost(post).stream()
                    .filter(c -> c.getFather()==null)
                    .collect(Collectors.toList());
        }
    }

    public void updateComment(long commentId) {
        var retry=true;
        while(retry) {
            retry=false;
            try {
                commentServiceFailure.updateComment(commentId);
            } catch (org.springframework.orm.ObjectOptimisticLockingFailureException e){
                retry=true;
            }
        }
    }
}
