package fr.uge.jee.reddit.service;

import fr.uge.jee.reddit.post.Post;
import fr.uge.jee.reddit.repository.UserRepository;
import fr.uge.jee.reddit.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import javax.transaction.Transactional;

@Service
public class UserEntityDetailsServiceFailure {

    @Autowired
    private UserRepository userRepository;

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public boolean insertUser(User user) {
        var newUser = em.find(User.class, user.getUsername());
        if (newUser == null) {
            userRepository.save(user);
            return true;
        }
        return false;
    }

    @Transactional
    public void addPost(Post post, User user) {
        var currentUser = em.find(User.class, user.getUsername());
        currentUser.addPost(post);
    }

    @Transactional
    public void updatePassword(String username, String np) {
        var user = em.find(User.class, username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        user.setPassword(np);
    }
}
