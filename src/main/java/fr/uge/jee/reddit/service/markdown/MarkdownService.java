package fr.uge.jee.reddit.service.markdown;

public interface MarkdownService {
    String markdownToHtml(String markdown);
    String markdownToPlainText(String markdown);
}
