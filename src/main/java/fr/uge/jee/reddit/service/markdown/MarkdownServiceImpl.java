package fr.uge.jee.reddit.service.markdown;

import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;
import org.commonmark.renderer.text.TextContentRenderer;
import org.springframework.stereotype.Service;

@Service
public class MarkdownServiceImpl implements MarkdownService {

    @Override
    public String markdownToHtml(String markdown) {
        var parser = Parser.builder().build();
        var document = parser.parse(markdown);
        var renderer = HtmlRenderer.builder()
                .attributeProviderFactory(attributeProviderContext -> new LinkAttributeProvider()).build();
        return renderer.render(document);
    }

    @Override
    public String markdownToPlainText(String markdown) {
        var parser = Parser.builder().build();
        var document = parser.parse(markdown);
        var renderer = TextContentRenderer.builder()
                .nodeRendererFactory(context -> new LinkRenderer(context))
                .build();
        return renderer.render(document);
    }
}
