package fr.uge.jee.reddit.service;

import fr.uge.jee.reddit.dto.PostDTO;
import fr.uge.jee.reddit.enums.SortCriteria;
import fr.uge.jee.reddit.post.Comment;
import fr.uge.jee.reddit.post.DisplayedContent;
import fr.uge.jee.reddit.post.Post;
import fr.uge.jee.reddit.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.Comparator;
import java.util.List;

@Service
public class PostService {
    @Autowired
    PostRepository postRepository;

    @Autowired
    PostServiceFailure postFailureService;

    @PersistenceContext
    EntityManager em;

    public PostService() {
    }


    public void addComment(long postId, Comment comment) {
        var retry=true;
        while(retry) {
            retry = false;
            try {
                postFailureService.addComment(postId, comment);
            } catch (org.springframework.orm.ObjectOptimisticLockingFailureException e ) {
                retry = true;
            }
        }
    }

    public Post loadPost(long postId) {
        return postRepository.loadPost(postId);
    }

    static int compareByScore(DisplayedContent p1, DisplayedContent p2) {
        if (p1.getScore() > p2.getScore()) {
            return -1;
        } else {
            return 1;
        }
    }

    static int compareByTime(DisplayedContent p1, DisplayedContent p2) {
        if (p1.getTimestamp() < p2.getTimestamp()) {
            return 1;
        } else if (p1.getTimestamp() > p2.getTimestamp()) {
            return -1;
        } else {
            return compareByScore(p1, p2);
        }
    }

    @Transactional
    public Page<PostDTO> loadPostDTOSortByScore(Pageable pageable) {
        var q = "SELECT new fr.uge.jee.reddit.dto.PostDTO(p.postId, p.title, p.author, p.content, p.voteManager, p.timestamp, SIZE(p.comments)) from Post p";
        var query = em.createQuery(q, PostDTO.class);
        var result = query
                .getResultList();
        result.sort(Comparator.comparingInt(PostDTO::getScore).reversed());

        PagedListHolder<PostDTO> page = new PagedListHolder<>(result);
        page.setPageSize(pageable.getPageSize());
        page.setPage(pageable.getPageNumber());

        return new PageImpl<>(page.getPageList(), pageable, result.size());
    }

    public Page<PostDTO> loadPostsDTO(SortCriteria sortCriteria, Pageable pageable) {
        switch(sortCriteria) {
            case CONTROVERSY:
            case TIME: return postRepository.loadPostDTOSortByTime(pageable);
            case SCORE: return loadPostDTOSortByScore(pageable);
            default: return postRepository.loadPostDTO(pageable);
        }
    }

    public PostDTO loadPostDTO(long postId) {
        return postRepository.loadPostDTO(postId);
    }

    public List<PostDTO> findPostsDTOByUser(String username) {
        return postRepository.findPostsDTOByUser(username);
    }

    public void addUpvote(long postId, String username) {
//        var retry=true;
//        while(retry) {
//            retry=false;
//            try {
//                postFailureService.addUpvote(postId, username);
//            } catch (ObjectOptimisticLockingFailureException e){
//                retry=true;
//            }
//        }
        postFailureService.addUpvote(postId, username);
    }

    public void addDownvote(long postId, String username) {
//        var retry=true;
//        while(retry) {
//            retry=false;
//            try {
//                postFailureService.addDownvote(postId, username);
//            } catch (ObjectOptimisticLockingFailureException e) {
//                retry=true;
//            }
//        }
        postFailureService.addDownvote(postId, username);
    }

    public void deletePostFromId(long id) {
        var retry=true;
        while(retry) {
            retry=false;
            try {
                postFailureService.deletePostFromId(id);
            } catch (ObjectOptimisticLockingFailureException e) {
                retry=true;
            }
        }
    }
}
