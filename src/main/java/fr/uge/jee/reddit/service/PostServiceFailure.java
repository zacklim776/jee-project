package fr.uge.jee.reddit.service;


import fr.uge.jee.reddit.post.Comment;
import fr.uge.jee.reddit.post.Post;
import fr.uge.jee.reddit.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.Objects;

@Service
public class PostServiceFailure {

    @Autowired
    PostRepository postRepository;

    @PersistenceContext
    EntityManager em;

    @Transactional
    public void addComment(long postId, Comment comment) {
        Objects.requireNonNull(comment);
        var post = em.find(Post.class, postId);
        if (post == null) {
            throw new IllegalStateException("post not found");
        }
        post.addComment(comment);
    }

    @Transactional
    public void addUpvote(long postId, String username) {
        var post = em.find(Post.class, postId, LockModeType.PESSIMISTIC_WRITE);
        if (post == null) {
            throw new IllegalStateException("post not found");
        }
        post.addUpvote(username.hashCode());
    }

    @Transactional
    public void addDownvote(long postId, String username) {
        var post = em.find(Post.class, postId, LockModeType.PESSIMISTIC_WRITE);
        if (post == null) {
            throw new IllegalStateException("post not found");
        }
        post.addDownVote(username.hashCode());
    }

    @Transactional
    public void deletePostFromId(long id) {
        var postToDelete = em.find(Post.class, id);
        if (postToDelete == null) {
            throw new IllegalArgumentException("post not found");
        }
        postRepository.delete(postToDelete);
    }
}
