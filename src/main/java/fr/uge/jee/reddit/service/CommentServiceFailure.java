package fr.uge.jee.reddit.service;

import fr.uge.jee.reddit.post.Comment;
import fr.uge.jee.reddit.post.Post;
import fr.uge.jee.reddit.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Service
public class CommentServiceFailure {

    @Autowired
    CommentRepository commentRepository;

    @PersistenceContext
    EntityManager em;

    @Transactional
    public void addComment(Comment comment) {
        commentRepository.save(comment);
    }

    @Transactional
    public void addUpvote(long id, String username) {
        var comment = em.find(Comment.class, id, LockModeType.PESSIMISTIC_WRITE);
        if (comment == null) {
            throw new IllegalStateException("comment not found");
        }
        comment.addUpvote(username.hashCode());
    }

    @Transactional
    public void addDownvote(long id, String username) {
        var comment = em.find(Comment.class, id, LockModeType.PESSIMISTIC_WRITE);
        if (comment == null) {
            throw new IllegalStateException("comment not found");
        }
        comment.addDownVote(username.hashCode());
    }

    @Transactional
    public void updateComment(long commentId) {
        var comment = em.find(Comment.class, commentId);
        if (comment == null) {
            throw new IllegalStateException("comment not found");
        }
        comment.setDeleted(true);
    }
}
