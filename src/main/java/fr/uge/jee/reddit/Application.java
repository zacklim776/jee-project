package fr.uge.jee.reddit;

import com.github.javafaker.Faker;
import fr.uge.jee.reddit.post.Comment;
import fr.uge.jee.reddit.post.Post;
import fr.uge.jee.reddit.service.CommentService;
import fr.uge.jee.reddit.service.PostService;
import fr.uge.jee.reddit.service.UserEntityDetailsService;
import fr.uge.jee.reddit.user.User;
import org.hibernate.PersistentObjectException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    public static String timeFormatBuilder(long timestamp) {
        var timeAfterCreation = Timestamp.from(Instant.now()).getTime() - timestamp;

        if (TimeUnit.MILLISECONDS.toDays(timeAfterCreation) > 0) {
            var days = TimeUnit.MILLISECONDS.toDays(timeAfterCreation);
            if (days > 365) {
                days /= 365;
                return " • " + days + " years";
            } else if (days > 30) {
                return " • " + days + " months";
            }
            return " • " + TimeUnit.MILLISECONDS.toDays(timeAfterCreation) + " days";
        }
        if (TimeUnit.MILLISECONDS.toHours(timeAfterCreation) > 0) {
            return " • " + TimeUnit.MILLISECONDS.toHours(timeAfterCreation) + " hours";
        } else if (TimeUnit.MILLISECONDS.toMinutes(timeAfterCreation) > 0) {
            return " • " + TimeUnit.MILLISECONDS.toMinutes(timeAfterCreation) + " minutes";
        } else {
            return " • " + TimeUnit.MILLISECONDS.toSeconds(timeAfterCreation) + " seconds";
        }
    }

    @Bean
    DataSource getDataSource() {
        var dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.url("jdbc:h2:tcp://localhost/~/h2DB");
        dataSourceBuilder.username("sa");
        dataSourceBuilder.password("");
        return dataSourceBuilder.build();
    }

    @Bean
    public static CommandLineRunner cmd(PostService postService, CommentService commentService, UserEntityDetailsService userDetailsService) {
        return args -> {
            var user = new User("admin",
                    WebSecurityConfig.passwordEncoder().encode("admin"),
                    "ROLE_ADMIN");
            userDetailsService.insertUser(user);
            var arnaud = new User("arnaud",
                    WebSecurityConfig.passwordEncoder().encode("arnaud"),
                    "ROLE_USER");
            userDetailsService.insertUser(arnaud);
            Faker faker = new Faker();
            var post1 = new Post(faker.chuckNorris().fact(), user, faker.chuckNorris().fact(), faker.date().birthday().getTime());
            userDetailsService.addPost(post1, user);

            var comment2 = new Comment(user, "thé thai", null, Timestamp.valueOf("2004-02-04 17:00:00").getTime());
            comment2.setOrigin(post1);
            postService.addComment(1L, comment2);


            var threads = new ArrayList<Thread>();
            for (int i = 0; i < 8; i++) {
                var thread=new Thread(() -> {
                    for (int j = 0; j < 1000; j++) {
                        var name = faker.name().username();
                        var u = new User(name, WebSecurityConfig.passwordEncoder().encode(name), "ROLE_USER");
                        while (!userDetailsService.insertUser(u)) {
                            name = faker.name().username();
                            u = new User(name, WebSecurityConfig.passwordEncoder().encode(name), "ROLE_USER");
                        }
                        for (int h = 0; h < 10; h++) {
                            var post = new Post(faker.chuckNorris().fact(), u, faker.chuckNorris().fact(), faker.date().birthday().getTime());
                            userDetailsService.addPost(post, u);
                            for (int k = 0; k < 10; k++) {
                                var comment = new Comment(u, faker.esports().game(), null, faker.date().birthday().getTime());
                                comment.setOrigin(post);
                                commentService.addComment(comment);
                            }
                        }
                        postService.addUpvote(post1.getPostId(), u.getUsername());
                    }
                });
                threads.add(thread);
                thread.start();
            }
            for(var thread : threads){
                thread.join();
            }
            System.out.println("done");
        };
    }

}
