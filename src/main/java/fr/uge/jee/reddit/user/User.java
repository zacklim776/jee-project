package fr.uge.jee.reddit.user;

import fr.uge.jee.reddit.post.Comment;
import fr.uge.jee.reddit.post.Post;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "users")
public class User implements Serializable {
    @NotEmpty(message = "username cannot be empty")
    @Pattern(regexp="^[A-Za-z][A-Za-z._0-9]{3,30}$", message = "username must start with a letter and must contain between 3 and 30 letters")
    @Id
    private String username;

    @NotEmpty(message = "password cannot be empty")
    private String password;

    private String authorities;

    private boolean enabled;

    @OneToMany(cascade = {CascadeType.PERSIST}, mappedBy = "author")
    private List<Post> posts = new ArrayList<>();

    @OneToMany(cascade = {CascadeType.PERSIST}, mappedBy = "author")
    private List<Comment> comments = new ArrayList<>();

    @Version
    private Long version;

    public User() {
        this.authorities = "ROLE_USER";
    }

    public User(String login, String password, String authorities) {
        this.username = login;
        this.password = password;
        this.enabled = true;
        this.authorities = authorities;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getAuthorities() {
        return authorities;
    }

    public void setEnabled() {
        this.enabled = true;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAuthorities(String authorities) {
        this.authorities = authorities;
    }

    public boolean hasAuthorities(String authorities) {
        return this.authorities.equals(authorities);
    }

    public void setComments(ArrayList<Comment> comments){
        this.comments = comments;
    }

    public void setPosts(ArrayList<Post> posts) {
        this.posts = posts;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void addPost(Post post) {
        posts.add(post);
    }

    public void removePost(long id) { posts.removeIf(p->p.getPostId()== id);}

    public void addComment(Comment comment) {
        comments.add(comment);
    }

    public int hash() {
        return username.hashCode();
    }
}
