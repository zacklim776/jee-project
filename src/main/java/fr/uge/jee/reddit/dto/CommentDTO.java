package fr.uge.jee.reddit.dto;

import fr.uge.jee.reddit.post.VoteManager;
import fr.uge.jee.reddit.user.User;

import static fr.uge.jee.reddit.Application.timeFormatBuilder;

public class CommentDTO {
    private long commentId;
    private User author;
    private String content;
    private boolean deleted;
    private VoteManager voteManager;
    private long timestamp;

    public CommentDTO(long commentId, User author, String content, boolean deleted, VoteManager voteManager, long timestamp) {
        this.commentId = commentId;
        this.author = author;
        this.content = content;
        this.deleted = deleted;
        this.voteManager = voteManager;
        this.timestamp = timestamp;
    }

    public long getCommentId() {
        return commentId;
    }

    public User getAuthor() {
        return author;
    }

    public String getContent() {
        return content;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public VoteManager getVoteManager() {
        return voteManager;
    }

    public int getScore() {
        return voteManager.getScore();
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String displayCreationDate() {
        return timeFormatBuilder(timestamp);
    }
}
