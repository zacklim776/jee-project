package fr.uge.jee.reddit.dto;

import fr.uge.jee.reddit.post.Comment;
import fr.uge.jee.reddit.post.DisplayedContent;
import fr.uge.jee.reddit.post.VoteManager;
import fr.uge.jee.reddit.user.User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static fr.uge.jee.reddit.Application.timeFormatBuilder;

public class PostDTO implements Serializable, DisplayedContent {

    private long postId;
    private String title;
    private User author;
    private String content;
    private VoteManager voteManager;
    private List<Comment> comments = new ArrayList<>();
    private long timestamp;
    private int numberOfComments;

    public PostDTO(long postId, String title, User author, String content, VoteManager voteManager, long timestamp, int size) {
        this.postId = postId;
        this.title = title;
        this.author = author;
        this.content = content;
        this.voteManager = voteManager;
        this.timestamp = timestamp;
        this.numberOfComments = size;
    }

    public long getPostId() {
        return postId;
    }

    public String getTitle() {
        return title;
    }

    public User getAuthor() {
        return author;
    }

    public String getContent() {
        return content;
    }

    public VoteManager getVoteManager() {
        return voteManager;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public int getScore() {
        return voteManager.getScore();
    }

    public long getTimestamp() {
        return timestamp;
    }

    public int getNumberOfComments() {
        return numberOfComments;
    }

    public String displayCreationDate() {
        return timeFormatBuilder(timestamp);
    }
}
