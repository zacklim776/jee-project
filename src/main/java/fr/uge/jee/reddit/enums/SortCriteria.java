package fr.uge.jee.reddit.enums;

public enum SortCriteria {
    DEFAULT,
    TIME,
    SCORE,
    CONTROVERSY,
}
