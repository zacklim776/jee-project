package fr.uge.jee.reddit.controller;

import fr.uge.jee.reddit.PasswordManager;
import fr.uge.jee.reddit.WebSecurityConfig;
import fr.uge.jee.reddit.dto.PostDTO;
import fr.uge.jee.reddit.enums.SortCriteria;
import fr.uge.jee.reddit.post.Comment;
import fr.uge.jee.reddit.post.Post;
import fr.uge.jee.reddit.service.CommentService;
import fr.uge.jee.reddit.service.PostService;
import fr.uge.jee.reddit.service.UserEntityDetailsService;
import fr.uge.jee.reddit.service.markdown.MarkdownService;
import fr.uge.jee.reddit.user.User;
import fr.uge.jee.reddit.user.UserEntityDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.function.BiConsumer;

@Controller
public class AppController {

    @Autowired
    private UserEntityDetailsService userEntityDetailsService;

    @Autowired
    private PostService postService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private MarkdownService markdownService;

    @GetMapping({"/home", "/"})
    public String homePage(HttpSession httpSession, Model model) {
        if (httpSession.isNew()) {
            //Might be useless
            httpSession.setAttribute("currentUser", new UserEntityDetails());
            httpSession.setAttribute("sortCriteria", SortCriteria.DEFAULT);
            httpSession.setAttribute("commentSortCriteria", SortCriteria.TIME);
            model.addAttribute("pageNum", 0);
        }
        var pageNum = model.getAttribute("pageNum");
        if (pageNum == null) {
            pageNum = 0;
            model.addAttribute("pageNum", pageNum);
        }

        var sortCriteriaObj = httpSession.getAttribute("sortCriteria");
        if (sortCriteriaObj != null) {
            var sortCriteria = (SortCriteria) sortCriteriaObj;
            httpSession.setAttribute("postsDTO", postService.loadPostsDTO(sortCriteria, PageRequest.of((int) pageNum, 10)));
        } else {
            httpSession.setAttribute("postsDTO", postService.loadPostsDTO(SortCriteria.DEFAULT, PageRequest.of((int) pageNum, 10)));
        }

        var obj = httpSession.getAttribute("postsDTO");
        Page<PostDTO> posts = null;
        if (obj != null) {
            posts = (Page<PostDTO>) obj;
            model.addAttribute("maxPage", posts.getTotalPages());
        }

        model.addAttribute("posts", posts);
        httpSession.setAttribute("currentPage", "home");

        addUserDetails(model);
        return "home";
    }

    @GetMapping({"/home/{pageNum}"})
    public String changePage(@PathVariable("pageNum") int pageNum, HttpSession httpSession, Model model) {
        model.addAttribute("pageNum", pageNum);
        return homePage(httpSession, model);
    }

    @GetMapping("/home/best")
    public String homeBest(HttpSession httpSession, Model model) {
        httpSession.setAttribute("sortCriteria", SortCriteria.SCORE);
        return homePage(httpSession, model);
    }

    @GetMapping("/home/new")
    public String homeNew(HttpSession httpSession, Model model) {
        httpSession.setAttribute("sortCriteria", SortCriteria.TIME);
        return homePage(httpSession, model);
    }

    private void addUserDetails(Model model) {
        Object objUser = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        UserDetails userDetails = new UserEntityDetails();

        if (objUser instanceof UserDetails) {
            userDetails = ((UserEntityDetails) objUser);
        }

        model.addAttribute("currentUser", userDetails);
    }

    @GetMapping("/login")
    public String auth(HttpSession httpSession, Model model) {
        return "auth";
    }

    @GetMapping("/sign")
    public String signIn(HttpSession httpSession, Model model) {
        model.addAttribute("user", new User());
        return "signUp";
    }

    @PostMapping("/sign")
    public String registerNewUser(@Valid @ModelAttribute("user") User user,
                                  BindingResult bindingResult,
                                  HttpSession httpSession,
                                  Model model
                                  ) {
        if (bindingResult.hasErrors()) {
            return "signUp";
        }
        user.setAuthorities("ROLE_USER");
        user.setEnabled();
        PasswordManager.checkPasswordRegexp(user.getPassword());
        var pwd = WebSecurityConfig.passwordEncoder().encode(user.getPassword());
        user.setPassword(pwd);
        if (!userEntityDetailsService.insertUser(user)) {
            model.addAttribute("usernameErr", "username already taken");
            return "signUp";
        }
        return homePage(httpSession, model);
    }

    @GetMapping("/post/{postId}")
    public String displayPost(@PathVariable("postId") long postId, HttpSession httpSession, Model model) {
        var post = postService.loadPost(postId);
        model.addAttribute("item", post);
        model.addAttribute("postSelected", true);

        var commentSortCriteriaObj = httpSession.getAttribute("commentSortCriteria");
        if(commentSortCriteriaObj != null) {
            var commentSortCriteria = (SortCriteria) commentSortCriteriaObj;
            model.addAttribute("commentList",
                    commentService.loadCommentsByPost(post, commentSortCriteria));
        }

        model.addAttribute("comment", new Comment());

        httpSession.setAttribute("currentPage", "post");

        addUserDetails(model);

        return "currentPost";
    }

    @GetMapping("/post/new")
    public String createPost(Model model) {
        model.addAttribute("post", new Post());
        return "postForm";
    }

    /**
     * Add a vote to the post or comment using a functional interface.
     * @param id id of the post or the comment to vote
     * @param consumer function that upvotes or downvotes a post or a comment
     * @return
     */
    private String vote(@AuthenticationPrincipal UserEntityDetails userDetails, long id, BiConsumer<Long, String> consumer) {
        var user = userEntityDetailsService.findUser(userDetails.getUsername());
        consumer.accept(id, user.getUsername());
        return "score";
    }

    /**
     * Add a vote to a post
     * @param userDetails
     * @param postId
     * @param httpSession
     * @param model
     * @param consumer
     * @return
     */
    private String votePost(@AuthenticationPrincipal UserEntityDetails userDetails, long postId, HttpSession httpSession, Model model, BiConsumer<Long, String> consumer) {
        if (userDetails == null) {
            return "auth";
        }

        //Security to avoid user from voting his own posts
        if (userDetails.getUsername().equals(postService.loadPost(postId).getAuthor().getUsername())) {
            return displayPost(postId, httpSession, model);
        }
        var page = vote(userDetails, postId, consumer);
        model.addAttribute("score", postService.loadPostDTO(postId).getScore());
        return page;
    }

    /**
     * Add a vote to a comment
     * @param userDetails
     * @param postId
     * @param commentId
     * @param httpSession
     * @param model
     * @param consumer
     * @return
     */
    private String voteComment(@AuthenticationPrincipal UserEntityDetails userDetails, long postId, long commentId, HttpSession httpSession, Model model, BiConsumer<Long, String> consumer) {
        if (userDetails == null) {
            return "auth";
        }
        var comment = commentService.getCommentById(commentId);

        //Security to avoid user from voting for his own comment
        if (comment.isPresent()) {
            if (userDetails.getUsername().equals(comment.get().getAuthor().getUsername())) {
                return displayPost(postId, httpSession, model);
            }
        }
        var page = vote(userDetails, commentId, consumer);
        model.addAttribute("score", commentService.getCommentDTOById(commentId).getScore());
        return page;
    }

    @PutMapping(value = {"/upvote/{postId}"})
    public String upvote(@PathVariable("postId") long postId,
                         @AuthenticationPrincipal UserEntityDetails userDetails,
                         HttpSession httpSession, Model model) {
        return votePost(userDetails, postId, httpSession, model, postService::addUpvote);
    }

    @PutMapping(value = {"/downvote/{postId}"})
    public String downvote(@PathVariable("postId") long postId,
                           @AuthenticationPrincipal UserEntityDetails userDetails,
                           HttpSession httpSession, Model model) {
        return votePost(userDetails, postId, httpSession, model, postService::addDownvote);
    }

    @PutMapping(value = {"/upvote/{postId}/{commentId}"})
    public String upvoteComment(@PathVariable("postId") long postId,
                                @PathVariable("commentId") long commentId,
                                @AuthenticationPrincipal UserEntityDetails userDetails,
                                HttpSession httpSession, Model model) {
        return voteComment(userDetails, postId, commentId, httpSession, model, commentService::addUpvote);
    }

    @PutMapping(value = {"/downvote/{postId}/{commentId}"})
    public String downvoteComment(@PathVariable("postId") long postId,
                                  @PathVariable("commentId") long commentId,
                                  @AuthenticationPrincipal UserEntityDetails userDetails,
                                  HttpSession httpSession, Model model) {
        return voteComment(userDetails, postId, commentId, httpSession, model, commentService::addDownvote);
    }

    @PostMapping("/post/delete/{postId}")
    public String deletePost(@ModelAttribute("post") Post post,
                             @PathVariable("postId") long postId,
                             @AuthenticationPrincipal UserEntityDetails userDetails,
                             HttpSession httpSession,
                             BindingResult bindingResult,
                             Model model) {
        if (userDetails == null) {
            return auth(httpSession, model);
        }
        if (postService.loadPost(postId) == null) {
            return "error";
        }

        if (userDetails.isAdmin() || postService.loadPost(postId).getAuthor().getUsername().equals(userDetails.getUsername())) {
            postService.deletePostFromId(postId);
        }

        return homePage(httpSession, model);
    }

    @PostMapping("/post/new")
    public String publishPost(@Valid @ModelAttribute("post") Post post,
                              BindingResult bindingResult,
                              @AuthenticationPrincipal UserEntityDetails userDetails,
                              HttpSession httpSession,
                              Model model) {
        if (bindingResult.hasErrors()) {
            return "postForm";
        }

        if (userDetails == null) {
            return "auth";
        }

        var postContent = post.getContent();
        var plainContent = markdownService.markdownToPlainText(postContent);
        var formattedContent = markdownService.markdownToHtml(postContent);

        post.setContent(plainContent);
        post.setFormattedContent(formattedContent);

        var user = userEntityDetailsService.findUser(userDetails.getUsername());
        post.setAuthor(user);
        userEntityDetailsService.addPost(post, user);
        return homePage(httpSession, model);
    }

    @PostMapping("/post/{postId}/newcomment")
    public String publishComment(@Valid @ModelAttribute("comment") Comment comment,
                                 BindingResult bindingResult,
                                 @PathVariable("postId") long postId,
                                 @AuthenticationPrincipal UserEntityDetails userDetails,
                                 HttpSession httpSession,
                                 Model model) {
        if (userDetails == null) {
            return "auth";
        }

        if (bindingResult.hasErrors()) {
            model.addAttribute("emptyComment", "please write something");
            return displayPost(postId, httpSession, model);
        }

        var commentContent = comment.getContent();
        var plainContent = markdownService.markdownToPlainText(commentContent);
        var formattedContent = markdownService.markdownToHtml(commentContent);

        comment.setContent(plainContent);
        comment.setFormattedContent(formattedContent);

        var user = userEntityDetailsService.findUser(userDetails.getUsername());
        var post = postService.loadPost(postId);
        comment.setAuthor(user);
        comment.setOrigin(post);
        postService.addComment(post.getPostId(), comment);
        return displayPost(postId, httpSession, model);
    }

    @GetMapping("/subcomment/{postId}/{commentId}")
    public String createSubCom(@AuthenticationPrincipal UserEntityDetails userDetails, @PathVariable("postId") long postId, @PathVariable("commentId") long commentId, HttpSession httpSession, Model model) {
        if (userDetails == null) {
            return "auth";
        }
        model.addAttribute("selectCommentId", commentId);
        model.addAttribute("newComment", new Comment());
        model.addAttribute("currentUser", userDetails);
        var post = postService.loadPost(postId);
        model.addAttribute("item", post);
        return "commentForm::subCommentForm";
    }

    @PostMapping("/subcomment/{postId}/{commentId}")
    public String publishSubCom(@Valid @ModelAttribute("comment") Comment comment,
                                BindingResult bindingResult, HttpSession httpSession,
                                @PathVariable("postId") long postId,
                                @PathVariable("commentId") long commentId,
                                @AuthenticationPrincipal UserEntityDetails userDetails,
                                Model model) {
        if (userDetails == null) {
            return "auth";
        }

        if (bindingResult.hasErrors()) {
            return displayPost(postId, httpSession, model);
        }

        var commentContent = comment.getContent();
        var plainContent = markdownService.markdownToPlainText(commentContent);
        var formattedContent = markdownService.markdownToHtml(commentContent);

        comment.setContent(plainContent);
        comment.setFormattedContent(formattedContent);

        var user = userEntityDetailsService.findUser(userDetails.getUsername());
        var father = commentService.getCommentById(commentId);
        var post = postService.loadPost(postId);
        if (father.isPresent()) {
            comment.setAuthor(user);
            comment.setFather(father.get());
            comment.setOrigin(post);
            postService.addComment(postId, comment);
        }

        model.addAttribute("selectCommentId", -1);
        return displayPost(postId, httpSession, model);
    }

    @PostMapping("/comment/delete/{postId}/{commentId}")
    public String deleteComment(@ModelAttribute("comment") Comment comment,
                                @PathVariable("postId") long postId,
                                @PathVariable("commentId") long commentId,
                                @AuthenticationPrincipal UserEntityDetails userDetails,
                                BindingResult bindingResult, HttpSession httpSession,
                                Model model) {
        var optComment = commentService.getCommentById(commentId);
        if (optComment.isEmpty()) {
            return "error";
        }

        if (!userDetails.isAdmin() && !userDetails.getUsername().equals(optComment.get().getAuthor().getUsername())) {
            return "error";
        }
        commentService.updateComment(commentId);
        return displayPost(postId, httpSession, model);
    }

    //Security when typing url in search bar
    @GetMapping({"/comment/delete/{postId}/{commentId}", "/post/{postId}/newcomment"})
    public String getMethodNotAllowed(HttpSession httpSession, Model model) {
        return homePage(httpSession, model);
    }

    @GetMapping("/user/{username}")
    public String showProfile(@PathVariable("username") String username,
                              @AuthenticationPrincipal UserEntityDetails userDetails,
                              HttpSession httpSession,
                              Model model) {
        if (userDetails == null) {
            return "auth";
        }
        addUserDetails(model);

        httpSession.setAttribute("userPostsDTO", postService.findPostsDTOByUser(username));
        var posts = (List<PostDTO>) httpSession.getAttribute("userPostsDTO");

        httpSession.setAttribute("userCommentsDTO", commentService.findCommentsByUser(username));
        var comments = (List<Comment>) httpSession.getAttribute("userCommentsDTO");

        model.addAttribute("userPosts", posts);
        model.addAttribute("userComments", comments);

        return "myProfile";
    }

    @GetMapping("/user/{username}/updatePassword")
    public String getPasswordUpdateForm(@PathVariable("username") String username,
                                        @AuthenticationPrincipal UserEntityDetails userDetails,
                                        HttpSession httpSession,
                                        Model model) {
        addUserDetails(model);
        model.addAttribute("passwordManager", new PasswordManager());

        return "updatePassword";
    }

    @PostMapping("/user/{username}/updatePassword")
    public String updatePassword(@PathVariable("username") String username,
                                 @ModelAttribute("currentUser") UserEntityDetails user,
                                 @RequestParam(value = "oldPassword") String oldPassword,
                                 @Valid @ModelAttribute("passwordManager") PasswordManager pm,
                                 @AuthenticationPrincipal UserEntityDetails userDetails,
                                 HttpSession httpSession,
                                 Model model) {
        if (userDetails == null) {
            return "auth";
        }

        if (!pm.checkIfSimilar()) {
            model.addAttribute("confP", "password doesn't match");
            return "updatePassword";
        }

        if (WebSecurityConfig.passwordEncoder().matches(oldPassword,userDetails.getPassword())) {
            var np = WebSecurityConfig.passwordEncoder().encode(pm.getNewP());
            userEntityDetailsService.updatePassword(userDetails.getUsername(), np);
        } else {
            model.addAttribute("oldPassword", "wrong old password");
            return "updatePassword";
        }

        return "myProfile";
    }
}
